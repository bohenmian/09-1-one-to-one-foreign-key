package com.twuc.webApp.domain.oneToOne;

import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class UserAndProfileTest extends JpaTestBase {
    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private UserProfileEntityRepository userProfileEntityRepository;

    @Test
    void should_save_user_and_profile() {
        // TODO
        //
        // 请书写一个测试实现如下的功能：
        //
        // Given 一个 UserEntity 和一个 UserProfileEntity。它们并没有持久化。
        // When 持久化 UserEntity
        // Then UserProfileEntity 也一同被持久化了。
        //
        // <--start--
        UserEntity userEntity = new UserEntity();
        UserProfileEntity userProfileEntity = new UserProfileEntity("tom", "jack");

        flushAndClear(em -> {
            userEntity.setUserProfileEntity(userProfileEntity);
            userEntityRepository.save(userEntity);
        });

        run(em -> {
            UserProfileEntity savedUserProfile = userProfileEntityRepository.findById(2L).orElseThrow(RuntimeException::new);
            assertEquals("tom", savedUserProfile.getFirstName());
        });
        // --end->
    }

    @Test
    void should_remove_parent_and_child() {
        // TODO
        //
        // 请书写一个测试：
        //
        // Given 一个持久化了的 UserEntity 和 UserProfileEntity
        // When 删除 UserEntity
        // Then UserProfileEntity 也被删除了
        //
        // <--start-
        flushAndClear(em -> {
            UserEntity userEntity = new UserEntity();
            UserProfileEntity userProfileEntity = new UserProfileEntity("tom", "jack");
            userEntity.setUserProfileEntity(userProfileEntity);
            userEntityRepository.save(userEntity);
        });

        flushAndClear(em -> userEntityRepository.deleteById(1L));

        run(em -> {
            assertEquals(0, userProfileEntityRepository.findAll().size());
        });
        // --end->
    }
}
